<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Evan McIntire</title>
        <link type='text/css' rel='stylesheet' href='style.css' />
    </head>
    <body>
        <div class='column'>
            <h1 id='name'> Evan McIntire </h1>
            <div>Student | Programmer</div>
            <p> Hi, I'm Evan McIntire, an american high school student who plans to major in Computer Science. </p>
            <h1 id='projects'>Projects</h1>
            <div>
                <h4><a href='http://nodedraw.com'>NodeDraw</a></h4>
                Node.js powered collabrative drawing program, to be released soon(ish)
            </div>
            <div>
                <h4><a href='https://poll.evanmcintire.com'>Poll</a></h4>
                PHP powered polling site (similar to strawpoll.me)
            </div>
            <div>
                <h4><a href='https://www.npmjs.com/package/windowfy'> Windowfy </a></h4>
                 jquery plugin that allows elements to be wrapped in a draggable window, with a feel similar to Windows
            </div>
            <div>
                <a href='https://dev.evanmcintire.com'> Various other projects </a>
            </div>
            <h1 id='contact'>Contact</h1>
            mcintire.evan@gmail.com<br/>
            <a href='https://www.github.com/mcintireevan'>Github</a>
        </div>
    </body>
</html>
